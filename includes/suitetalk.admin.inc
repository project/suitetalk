<?php

/**
 * @file
 * Administrative UI and functions for the SuiteTalk module.
 */

/**
 * Form builder: Main administrative form.
 */
function suitetalk_admin_form($form, &$form_state) {
  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Credentials'),
  );
  $form['credentials']['suitetalk_config_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#default_value' => variable_get('suitetalk_config_endpoint', SUITETALK_CONFIG_DEFAULT_ENDPOINT),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('suitetalk_config_host', SUITETALK_CONFIG_DEFAULT_HOST),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#default_value' => variable_get('suitetalk_config_account'),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#default_value' => variable_get('suitetalk_config_app_id'),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('suitetalk_config_username'),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('suitetalk_config_password'),
    '#required' => TRUE,
  );
  $form['credentials']['suitetalk_config_role'] = array(
    '#type' => 'textfield',
    '#title' => t('Role'),
    '#default_value' => variable_get('suitetalk_config_role', SUITETALK_CONFIG_DEFAULT_ROLE),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
