# SuiteTalk

The SuiteTalk module integrates with the cloud business software, [NetSuite](http://www.netsuite.com) through its [web services API, SuiteTalk](http://www.netsuite.com/portal/developers/resources/suitetalk-sample-applications.shtml).

## Features

* Autoloading NetSuite PHP API Client;
* Migrate source class to an item saved search in NetSuite.
* Migrate source class to custom records in NetSuite.

## Requirements

* [Libraries API](https://www.drupal.org/project/libraries);
* [X Autoload](https://www.drupal.org/project/xautoload);
* [ryanwinchester/netsuite-php:2016.2.0](https://github.com/ryanwinchester/netsuite-php/releases/tag/v2016.2.0).

## Supporting Organizations

[Superior International Industries](http://superiorrecreationalproducts.com) — Funding  
[Bluespark](https://www.drupal.org/bluespark) — Development and Maintenance
