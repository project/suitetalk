<?php

namespace Drupal\suitetalk\Migrate;

use NetSuite\Classes\ItemSearchAdvanced;
use NetSuite\Classes\SearchRequest;

/**
 * @file
 * Provides a Migrate source class to an item saved search in NetSuite.
 */

/**
 * Migrate source class to retrieve results of an item saved search in NetSuite.
 */
class SuiteTalkSourceItemSavedSearch extends SuiteTalkSourceSearch {

  /**
   * Script ID of the saved search in NetSuite.
   *
   * @var string
   */
  protected $savedSearchScriptId;

  /**
   * Name of the class that represents the item type.
   *
   * @var string
   */
  protected $itemTypeClass;

  /**
   * Custom fields for mapping.
   *
   * @var array
   */
  protected $customFields;

  /**
   * Constructor for the migrate source class.
   *
   * @param string $saved_search_script_id
   *   Script ID of the saved search in NetSuite.
   * @param string $item_type_class
   *   Name of the class that represents the item type in which the search will
   *   return results. Don't include namespace.
   * @param array $custom_fields
   *   Custom fields for mapping.
   *
   * @todo Figure out how to retrieve custom item fields from NetSuite.
   */
  public function __construct($saved_search_script_id, $item_type_class, array $custom_fields = array()) {
    $this->savedSearchScriptId = $saved_search_script_id;
    $this->itemTypeClass = $item_type_class;
    $this->customFields = $custom_fields;
    parent::__construct();
  }

  /**
   * Implementation of SuiteTalkSourceSearch::searchRequest().
   *
   * Performs search request to retrieve the custom records.
   */
  protected function searchRequest() {
    $search = new ItemSearchAdvanced();
    $search->savedSearchScriptId = $this->savedSearchScriptId;

    $request = new SearchRequest();
    $request->searchRecord = $search;

    $response = $this->service->search($request);
    if ($response->searchResult->status->isSuccess) {
      return $response->searchResult;
    }
    return FALSE;
  }

  /**
   * Implementation of MigrateSource::fields().
   */
  public function fields() {
    $fields = array();
    $class = '\NetSuite\Classes\\' . $this->itemTypeClass;
    if (isset($class::$paramtypesmap)) {
      foreach ($class::$paramtypesmap as $property_name => $type) {
        $fields[$property_name] = $property_name;
      }
    }
    return array_merge($fields, $this->customFields);
  }

  /**
   * Returns a string describing the source.
   *
   * @return string
   *   Describes migration source.
   */
  public function __toString() {
    return t('Result of a saved search in NetSuite. Saved search ID: !id.', array('!id' => $this->savedSearchScriptId));
  }

}
