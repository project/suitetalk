<?php

namespace Drupal\suitetalk\Migrate;

use NetSuite\Classes\CustomRecordRef;
use NetSuite\Classes\CustomRecordSearchBasic;
use NetSuite\Classes\SearchRequest;

/**
 * @file
 * Provides a Migrate source class to custom records in NetSuite.
 */

/**
 * Migrate source class to retrieve custom records from NetSuite.
 */
class SuiteTalkSourceCustomRecords extends SuiteTalkSourceSearch {

  /**
   * Internal ID of custom record type in NetSuite.
   *
   * @var string
   */
  protected $customRecordTypeInternalID;

  /**
   * Constructor for the migrate source class.
   *
   * @param string $custom_record_type_internal_id
   *   Internal ID of custom record type in NetSuite.
   */
  public function __construct($custom_record_type_internal_id) {
    $this->customRecordTypeInternalID = $custom_record_type_internal_id;
    parent::__construct();
  }

  /**
   * Implementation of SuiteTalkSourceSearch::searchRequest().
   *
   * Performs search request to retrieve the custom records.
   */
  protected function searchRequest() {
    $search = new CustomRecordSearchBasic();
    $search->recType = new CustomRecordRef();
    $search->recType->internalId = $this->customRecordTypeInternalID;

    $request = new SearchRequest();
    $request->searchRecord = $search;

    $response = $this->service->search($request);
    if ($response->searchResult->status->isSuccess) {
      return $response->searchResult;
    }
    return FALSE;
  }

  /**
   * Implementation of MigrateSource::fields().
   */
  public function fields() {
    $fields = array();
    $record_type = suitetalk_retrieve_record('customRecordType', $this->customRecordTypeInternalID);
    if ($record_type) {
      foreach ($record_type->customFieldList->customField as $custom_field) {
        $fields['custom_' . $custom_field->scriptId] = $custom_field->label;
      }
    }
    return $fields;
  }

  /**
   * Returns a string describing the source.
   *
   * @return string
   *   Describes migration source.
   */
  public function __toString() {
    return t('Custom records from NetSuite. Custom record type ID: !id.', array('!id' => $this->customRecordTypeInternalID));
  }

}
