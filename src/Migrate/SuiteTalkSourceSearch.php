<?php

namespace Drupal\suitetalk\Migrate;

use NetSuite\Classes\SearchMoreWithIdRequest;

/**
 * @file
 * Provides an abstract Migrate source class for a NetSuite search response.
 */

/**
 * Migrate source class for a NetSuite search response.
 */
abstract class SuiteTalkSourceSearch extends \MigrateSource {

  /**
   * NetSuite service object.
   *
   * @var \NetSuite\NetSuiteService
   */
  protected $service;

  /**
   * Search result of the current request.
   *
   * @var \NetSuite\Classes\SearchResult
   */
  protected $searchResult = NULL;

  /**
   * Record index within the current page.
   *
   * @var int
   */
  protected $recordIndex = 0;

  /**
   * Constructor for the migrate source class.
   *
   * @param int $page_size
   *   Number of records to receive per search request.
   */
  public function __construct($page_size = 20) {
    parent::__construct();
    $this->service = suitetalk_get_service_object();
    $this->service->setSearchPreferences(FALSE, $page_size, FALSE);

    if (is_null($this->searchResult)) {
      $this->searchResult = $this->searchRequest();
    }
  }

  /**
   * Implement to perform the search request.
   *
   * @return \NetSuite\Classes\SearchResult
   *   NetSuite search result.
   */
  abstract protected function searchRequest();

  /**
   * Implementation of MigrateSource::getNextRow().
   *
   * @return object
   *   Data containing the next row in the migration.
   */
  public function getNextRow() {
    migrate_instrument_start('SuiteTalkSourceSearch::next');
    if ($row = $this->getNextRowWithinPage()) {
      return $row;
    }
    elseif ($this->getNextPage()) {
      return $this->getNextRowWithinPage();
    }
    else {
      migrate_instrument_stop('SuiteTalkSourceSearch::next');
      return NULL;
    }
  }

  /**
   * Returns the next row in the migration.
   *
   * @return bool|object
   *   Next row in the migration or false, if we are at the end of the current
   *   page in the search result.
   */
  protected function getNextRowWithinPage() {
    if (isset($this->searchResult->recordList->record[$this->recordIndex])) {
      $row = $this->searchResult->recordList->record[$this->recordIndex];
      if (isset($row->customFieldList)) {
        $this->flattenCustomNetSuiteFields($row);
      }
      $row = (object) get_object_vars($row);
      $this->recordIndex++;
      return $row;
    }
    return FALSE;
  }

  /**
   * Performs request to retrieve the next search result page.
   *
   * @return bool
   *   Whether there is a next page in the search results and that is has been
   *   successfully retrieved.
   */
  protected function getNextPage() {
    if ($this->searchResult->pageIndex < $this->searchResult->totalPages) {
      $this->recordIndex = 0;
      $new_request = new SearchMoreWithIdRequest();
      $new_request->searchId = $this->searchResult->searchId;
      $new_request->pageIndex = $this->searchResult->pageIndex + 1;
      $new_response = $this->service->searchMoreWithId($new_request);
      $this->searchResult = $new_response->searchResult;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Implementation of MigrateSource::performRewind().
   */
  public function performRewind() {
    $this->searchResult = $this->searchRequest();
    $this->recordIndex = 0;
  }

  /**
   * Implementation of MigrateSource::computeCount().
   *
   * @return int
   *   Number of total records the search yielded.
   */
  public function computeCount() {
    return $this->searchResult->totalRecords;
  }

  /**
   * Flattens custom NetSuite fields within a record.
   *
   * Useful to be able to provide those fields for mapping.
   *
   * @param \NetSuite\Classes\CustomRecord|\NetSuite\Classes\Record $record
   *   Record to flatten the custom fields on.
   */
  protected function flattenCustomNetSuiteFields(&$record) {
    foreach ($record->customFieldList->customField as $custom_field) {
      $name = 'custom_' . $custom_field->scriptId;
      $record->{$name} = $custom_field;
    }
  }

}
